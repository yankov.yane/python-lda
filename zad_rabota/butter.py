from scipy.signal import butter, lfilter


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype="band")
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    float_numbers = []
    for item in data:
        float_numbers.append(float(item.replace(",", "")))

    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, float_numbers)
    return y
