from configparser import ConfigParser
from scipy import signal
import butter
import numpy
import math
import datetime
from isNum_trunc import *
from Train import *


t = datetime.datetime.now().strftime(
    "Date: %d/%m/%Y  | Time: %H:%M:%S"
)  # Date and time of machine


def checkColumns(columns_input0, columns):
    # Ако сме задали "All" в "columns" в конфигурационния файл,
    # всички стойности в dictionary: columns стават True

    if columns_input0[0] == "All":
        for i in columns:
            columns[i] = True

    # Проверяваме кои колони са ни True за да позоволим да добавяме в тях
    else:
        for i in columns:
            for j in columns_input0:
                if i == j:
                    columns[i] = True


def getData():
    """
        Четем от .ini файла конфигурациите и ги слагаме в променливи
        """
    parser = ConfigParser()
    parser.read("config.ini")

    columns_input = parser.get("data", "columns").split(",")
    path_open = parser.get("data", "path_open")
    path_write = parser.get("data", "path_write")
    condition = parser.get("data", "event_ID").split(",")
    condition_after_event_ID = parser.get("data", "event_ID_after_event_ID")

    filter_order = int(parser.get("data", "filter_order"))
    fs = int(parser.get("data", "fs"))
    lowcut = int(parser.get("data", "lowcut"))
    highcut = int(parser.get("data", "highcut"))

    offset = float(parser.get("data", "offset"))
    epoch_duration = float(parser.get("data", "epoch_duration"))

    number_action = parser.get("data", "number_action")
    test_path = parser.get("data", "test_path")
    train_path = parser.get("data", "train_path")
    direction = parser.get("data", "direction")

    columns_to_test = parser.get("data", "columns_to_test").split(",")
    rows_to_chunk_tests = parser.get("data", "rows_to_chunk_tests")
    return (
        columns_input,
        path_open,
        path_write,
        condition,
        condition_after_event_ID,
        filter_order,
        fs,
        lowcut,
        highcut,
        offset,
        epoch_duration,
        number_action,
        train_path,
        test_path,
        direction,
        columns_to_test,
        rows_to_chunk_tests,
    )


def getData_less():
    parser = ConfigParser()
    parser.read("config.ini")

    path_open_test = parser.get("data", "path_open_test")
    path_append = parser.get("data", "path_append")
    return path_open_test, path_append


def get_columns_true_count(af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4):
    array_columns = [af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4]
    """
    2.0.1 - Намираме колко колони сме избрали(ще ни трябва в следващото .csv)
    """
    colums_true_count2 = 0
    for i in array_columns:
        if i[0] != "":
            colums_true_count2 += 1
    return colums_true_count2


def generate_Data_To_Train(
    path_write, array_data_average, path_train, without_names=False
):
    """
    Проверяваме дали сме задали файл
    """
    if path_train == "Empty":  # draw 5.1
        path_train = path_write + "info_train.csv"  # draw 5.2

        with open(path_write + "info_train.csv", "wt") as out:  # draw 5.3
            id_number_for_placing_at_start_of_line = ""
            last_direction = ""
            for item in zip(array_data_average):  # draw 5.4

                """
                Сменяваме eventID с думи и добавяме след тях същата дума когато е празно полето за да работи тренировката(не работи ако са числа)
                За да смени стойностите как трябва, файла който подаваме трябва да е структуриран по еднакъв начин както генерирания по default(този тука)
                """
                count_till_last_element = 0
                
                for i in item:  # draw 5.6
                    if i[0] != "":  # draw 5.7
                        if i[0] == "33025":  # draw 5.8
                            i[0] = "Left"  # draw 5.9
                            last_direction = "Left"
                        if i[0] == "33026":  # draw 5.10
                            i[0] = "Right"  # draw 5.11
                            last_direction = "Right"
                        id_number_for_placing_at_start_of_line = i[0]  # draw 5.12
                    else:
                        i[0] = last_direction

                    for j in i:  # draw 5.13
                        if j == "" and without_names == False:  # draw 5.14
                            j = id_number_for_placing_at_start_of_line  # draw 5.15

                        out.write(str(j) + ",")  # draw 5.16
                out.write("\n")  # draw 5.17
        return path_train  # draw 5.18
    else:
        return path_train  # draw 5.18


def write_DSP_Calculated_Data(
    offset,
    epoch_duration,
    condition,
    array_data_average,
    path_write,
    first_line_dsp,
    butter_data_path,
    colums_true_count,
):

    with open(butter_data_path, "r") as reader:  # draw 4.1
        a = reader.readline().split(",")

        """
        Превъщаме секундите в цяло число, което представлява редовете които ще трябва да пропуснем и групираме
        """
        offset_to_lines = int(offset * 128)
        epoch_duration_to_lines = int(epoch_duration * 128)

        count_line = 1
        count_added_to_average = 0
        allow_offset = False

        data_average = []
        array_average_to_be_calculated = [0.0] * colums_true_count
        id_number_for_placing_at_start_of_line = ""

        lines_readed = 0
        for line in reader:  # draw 4.2


            #if line.split(',')[-2] == '33026.0'or line.split(',')[-2] == '33025.0':
            #    array_data_average.append(lines_readed)
            #    lines_readed = 0
            #
            #lines_readed += 1
            """
                3.3 - Ако редовете които следим съвпаднат като бройка с нашия input спираме да игнорираме четеца и започваме да групираме
                """
            if count_line == offset_to_lines:  # draw 4.3
                allow_offset = False  # draw 4.4
                count_line = 1  # draw 4.4

            """
                3.2 - Ако имаме Offset четем нов ред и следим колко редове сме минали
                """
            if allow_offset == True:  # draw 4.5
                count_line += 1  # draw 4.6
                continue

            array_of_data = line.split(",")
            """
                3.1 - Проверяваме дали Еvent ID ни съвпада с прочетеното ID, ако съвпада започваме да четем, ако имаме Offset го пропускаме. Offset ще има докато не минем offset_to_lines реда
                """
            for id in condition:  # draw 4.7
                if array_of_data[(len(a) - 2)] == id + ".0":#"\n":  # draw 4.8
                    if offset_to_lines != 0:  # draw 4.9
                        allow_offset = True  # draw 4.10


                    ####### Calculate remaining data ( remaingin data that didn't pass the " if count = epoch_duration_to_lines " )
                    if count_added_to_average != 0: #or array_average_to_be_calculated != 0:
                        for i in array_average_to_be_calculated: 
                            data_average.append( 
                                truncate(math.log(1 + (i / epoch_duration_to_lines)), 5)
                               )
                        array_average_to_be_calculated = [0.0] * colums_true_count
                        data_average.insert(0, "")
                        array_data_average.append(data_average)
                        data_average = []
                        count_added_to_average = 0
                    ###################################


                    data_average.append(id)  # draw 4.11
                    id_number_for_placing_at_start_of_line = id  # draw 4.11

            """
                3.4 - Смятаме Simple DSP(x*x) за всяко число в групата, групата се състои от (epoch_duration_to_lines) данни за всяка колона, ако сме към края на Event и данните са ни по-малко от (epoch_duration_to_lines) изпозлваме колкото има.
                """
            if (count_added_to_average <= epoch_duration_to_lines):  # draw 4.12

                array_data_to_group = array_of_data[2 : len(array_of_data) - 2]
                count_added_to_average += 1

                for i in range(len(array_data_to_group)):  # draw 4.13
                    array_average_to_be_calculated[i] += float(array_data_to_group[i]) * float(array_data_to_group[i])  # draw 4.14

                    # ako imame edin ostanal red da se grupira kakvo pravim


            """
                3.5 - Правим средна стойност за всяда група стойности които разделихме.
                Правим DSP(log(1+x)) на средната стойност 
                """
            if count_added_to_average == epoch_duration_to_lines:  # draw 4.15
                for i in array_average_to_be_calculated:  # draw 4.16

                    #                   ||            ||
                    #         draw 4.18 \/ #draw 4.17 \/
                    data_average.append( 
                        truncate(math.log(1 + (i / epoch_duration_to_lines)), 5)
                       )
                array_average_to_be_calculated = [0.0] * colums_true_count
                """
                    3.6 - Проверяваме дали сме на реда с event_ID, ако не, добавяме празна стойност.
                    Трием стойностите в data_average, за да сметнем нова поредица от данни
                    """
                if len(data_average) == colums_true_count:  # draw 4.19
                    data_average.insert(0, "")  # za da e prazno v nachaloto na reda
                array_data_average.append(data_average)
                data_average = []
                count_added_to_average = 0

    """
    3.7 - Записваме получените данни в .csv 
    """
    with open(path_write + "DSP_Calculated_Data.csv", "wt") as out:  # draw 4.20
        out.write("Event ID" + first_line_dsp[10:] + t + "\n")  # draw 4.21
        for item in zip(array_data_average):  # draw 4.22
            for i in item:
                for j in i:
                    out.write(str(j) + ",")
            out.write("\n")

    return first_line_dsp[11:]  # draw 4.23


def generate_Butter_Data(
    path_write,
    first_line,
    af3,
    f7,
    f3,
    f5,
    t7,
    p7,
    o1,
    o2,
    p8,
    t8,
    fc6,
    f4,
    f8,
    af4,
    lowcut,
    highcut,
    fs,
    filter_order,
    filtered_signals,
    time,
    epoch,
    event_id,
    colums_true_count,
    columns_input,
):
    #with open(path_write + "ButterData.csv", "wt") as out:  # draw 3.2
    data_frame = pd.read_csv(path_write + "RawData.csv")

    if columns_input == ["All"]:
        columns_input = [  'AF3','F7','F3','F5','T7','P7','O1','O2','P8','T8','FC6','F4','F8','AF4'  ]

    for column in columns_input:
        array_to_butter = []
        for item in list(data_frame[column]):
            array_to_butter.append(str(item) )
        result_filtering = butter.butter_bandpass_filter(array_to_butter  , 8, 30, 128, 4)  # draw 3.8
        df=pd.DataFrame(result_filtering, columns=['Butter'])
        data_frame[column] = df["Butter"]

    data_frame.to_csv(path_write + "ButterData.csv", index=False) 
    
    butter_data_path = path_write + "ButterData.csv" 
    return colums_true_count, butter_data_path   # draw 3.14


def create_csv(
    af3,
    f7,
    f3,
    f5,
    t7,
    p7,
    o1,
    o2,
    p8,
    t8,
    fc6,
    f4,
    f8,
    af4,
    path,
    event_id,
    time,
    epoch,
    first_line="0",
):
    with open(path, "wt") as out:
        if first_line != "0":  # draw 2.1
            out.write(first_line)  # draw 2.2

        """
        1.6 - Зипваме файла за да не се чете като масив а като редове от .csv
        """
        for item in zip(
            time,
            epoch,
            af3,
            f7,
            f3,
            f5,
            t7,
            p7,
            o1,
            o2,
            p8,
            t8,
            fc6,
            f4,
            f8,
            af4,
            event_id,
        ):  # draw 2.3
            for i in item:  # draw 2.4
                out.write(i)

            out.write("\n")


def open_first_file(
    path_open,
    allow_write,
    time,
    epoch,
    columns,
    af3,
    f7,
    f3,
    f5,
    t7,
    p7,
    o1,
    o2,
    p8,
    t8,
    fc6,
    f4,
    f8,
    af4,
    event_id="",
    first_line_dsp="",
    first_line="",
    condition="",
    condition_after_event_ID="",
    need_backn=False,
):

    difference_index = 0  # draw 1.2
    if event_id == "":  # draw 1.1
        difference_index += 2  # draw 1.3

    counterLines = 0
    with open(path_open, "r") as file:  # draw 1.4

        """
        1.1 - Четем първия ред
        """
        line = file.readline().split(",")  # draw 1.5

        # Образуваме първия ред в .csv файла
        if line[0] == "Time:128Hz":  # draw 1.6
            # draw 1.7 ot
            first_line += "Time" + ","
            first_line += "Epoch" + ","
            for i in line:
                if columns["AF3"] == True and i == "AF3":
                    first_line += i + ","
                if columns["F7"] == True and i == "F7":
                    first_line += i + ","
                if columns["F3"] == True and i == "F3":
                    first_line += i + ","
                if columns["F5"] == True and i == "F5":
                    first_line += i + ","
                if columns["T7"] == True and i == "T7":
                    first_line += i + ","
                if columns["P7"] == True and i == "P7":
                    first_line += i + ","
                if columns["O1"] == True and i == "O1":
                    first_line += i + ","
                if columns["O2"] == True and i == "O2":
                    first_line += i + ","
                if columns["P8"] == True and i == "P8":
                    first_line += i + ","
                if columns["T8"] == True and i == "T8":
                    first_line += i + ","
                if columns["FC6"] == True and i == "FC6":
                    first_line += i + ","
                if columns["F4"] == True and i == "F4":
                    first_line += i + ","
                if columns["F8"] == True and i == "F8":
                    first_line += i + ","
                if columns["AF4"] == True and i == "AF4":
                    first_line += i + ","
            first_line_dsp = first_line
            first_line += "Event ID,"
            first_line += t + "\n"
            # draw 1.7 do
        """
        1.2 - Четем всеки ред във файла, проверяваме дали ID-то съвпада с някое от наще, ако съвпадн позволяваме да добавя данни в масивите,
        """
        for line in file:  # draw 1.8
            array = line.split(",")

            if (
                event_id != ""
                and first_line_dsp != ""
                and first_line != ""
                and condition != ""
                and condition_after_event_ID != ""
            ):  # draw 1.9

                for i in condition:  # draw 1.10

                    """
                    1.3 - Ако добавяме и намерим event_ID което сме задали като допълнение,не спираме да добавяме а продължаваме да пълним масивите
                    """
                    if allow_write == True:  # draw 1.11
                        if array[16] == condition_after_event_ID:  # draw 1.12
                            break
                    """
                    1.3.1 - Ако Event ID полето в колоната е другo event_ID не позволяваме да добавяме
                    """
                    if array[16] != "" and array[16] != i:  # draw 1.13
                        allow_write = False  # draw 1.14
                    if array[16] == i:  # draw 1.15
                        allow_write = True  # draw 1.16
                        break
            """
            1.4 - Добавяме във всeки True масив стойност, ако е False добавяме празна стойност за .csv файла
            """
            if allow_write == True:  # draw 1.17

                # draw 1.18 ot
                time.append(array[0] + ",")
                epoch.append(array[1] + ",")
                if columns["AF3"] == True:
                    af3.append(array[2 - difference_index] + ",")
                if columns["F7"] == True:
                    f7.append(array[3 - difference_index] + ",")
                if columns["F3"] == True:
                    f3.append(array[4 - difference_index] + ",")
                if columns["F5"] == True:
                    f5.append(array[5 - difference_index] + ",")
                if columns["T7"] == True:
                    t7.append(array[6 - difference_index] + ",")
                if columns["P7"] == True:
                    p7.append(array[7 - difference_index] + ",")
                if columns["O1"] == True:
                    o1.append(array[8 - difference_index] + ",")
                if columns["O2"] == True:
                    o2.append(array[9 - difference_index] + ",")
                if columns["P8"] == True:
                    p8.append(array[10 - difference_index] + ",")
                if columns["T8"] == True:
                    t8.append(array[11 - difference_index] + ",")
                if columns["FC6"] == True:
                    fc6.append(array[12 - difference_index] + ",")
                if columns["F4"] == True:
                    f4.append(array[13 - difference_index] + ",")
                if columns["F8"] == True:
                    f8.append(array[14 - difference_index] + ",")
                if columns["AF4"] == True:
                    if need_backn == False:
                        af4.append(array[15 - difference_index] + ",")
                    elif need_backn == True:
                        af4.append(array[15 - difference_index][:-2] + ",")

                if columns["AF3"] == False:
                    af3.append("")
                if columns["F7"] == False:
                    f7.append("")
                if columns["F3"] == False:
                    f3.append("")
                if columns["F5"] == False:
                    f5.append("")
                if columns["T7"] == False:
                    t7.append("")
                if columns["P7"] == False:
                    p7.append("")
                if columns["O1"] == False:
                    o1.append("")
                if columns["O2"] == False:
                    o2.append("")
                if columns["P8"] == False:
                    p8.append("")
                if columns["T8"] == False:
                    t8.append("")
                if columns["FC6"] == False:
                    fc6.append("")
                if columns["F4"] == False:
                    f4.append("")
                if columns["F8"] == False:
                    f8.append("")
                if columns["AF4"] == False:
                    af4.append("")
                if event_id != "":
                    event_id.append(array[16])
                # draw 1.18 do
        if (
            event_id == ""
            or first_line_dsp == ""
            or first_line == ""
            or condition == ""
            or condition_after_event_ID == ""
        ):  # draw 1.19
            count_of_data_test = 0

            # counterLines += 1
            # if counterLines == 50000:
            #    break

            for item in af3:
                count_of_data_test += 1

            return (
                af3,
                f7,
                f3,
                f5,
                t7,
                p7,
                o1,
                o2,
                p8,
                t8,
                fc6,
                f4,
                f8,
                af4,
                count_of_data_test,
            )  # draw 1.20
        return (
            first_line_dsp,
            first_line,
            af3,
            f7,
            f3,
            f5,
            t7,
            p7,
            o1,
            o2,
            p8,
            t8,
            fc6,
            f4,
            f8,
            af4,
        )  # draw 1.21


def main():
    # Всичките данни от конфигурационния файл
    (
        columns_input,
        path_open,
        path_write,
        condition,
        condition_after_event_ID,
        filter_order,
        fs,
        lowcut,
        highcut,
        offset,
        epoch_duration,
        number_action,
        train_path,
        test_path,
        direction,
        columns_to_test,
        rows_to_chunk_tests,
    ) = getData()  # draw 0.0

    # columns е dictionary с колоните от .csv файла
    columns = {
        "AF3": False,
        "F7": False,
        "F3": False,
        "F5": False,
        "T7": False,
        "P7": False,
        "O1": False,
        "O2": False,
        "P8": False,
        "T8": False,
        "FC6": False,
        "F4": False,
        "F8": False,
        "AF4": False,
    }

    # Проверяваме кои колони ще изполваме
    checkColumns(columns_input, columns)  # draw 0.1

    # Масиви които представляват всяка колона от .csv
    time, epoch = [], []
    af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4 = (
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    )
    event_id = []

    # Променлива с която определяме дали ще пълним масивите(тези от горе)
    allow_write = False

    first_line = ""
    first_line_dsp = ""

    # 1 - Отваряме посочения файл от config.ini файла
    (
        first_line_dsp,
        first_line,
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
    ) = open_first_file(
        path_open,
        allow_write,
        time,
        epoch,
        columns,
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
        event_id,
        first_line_dsp,
        first_line,
        condition,
        condition_after_event_ID,
    )  # draw 1.0

    raw_data_path = path_write + "RawData.csv"  # draw 0.2

    # 1.5 - Генерираме ново .csv с данните които имаме след изчистването на ненужните
    create_csv(
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
        raw_data_path,
        event_id,
        time,
        epoch,
        first_line,
    )  # draw 2.0

    colums_true_count = 0
    filtered_signals = []

    if number_action != "2":  # draw 0.3
        # 2.0 - Генерираме ново .csv за Butterworth филтрираните данни
        colums_true_count, raw_data_path = generate_Butter_Data(
            path_write,
            first_line,
            af3,
            f7,
            f3,
            f5,
            t7,
            p7,
            o1,
            o2,
            p8,
            t8,
            fc6,
            f4,
            f8,
            af4,
            lowcut,
            highcut,
            fs,
            filter_order,
            filtered_signals,
            time,
            epoch,
            event_id,
            colums_true_count,
            columns_input,
        )  # draw 3.0

    array_data_average = []

    colums_true_count = get_columns_true_count(
        af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4
    )  # draw 0.3

    if number_action != "1":  # draw 0.4
        # 3.0 - Отваряме предишния .csv файл, правим DSP сметки и ги записваме в ново .csv
        first_line_DSP = write_DSP_Calculated_Data(
            offset,
            epoch_duration,
            condition,
            array_data_average,
            path_write,
            first_line_dsp,
            raw_data_path,
            colums_true_count,
        )  # draw 4.0

    # 4.0 - Генерираме файла за трениране на мрежата
    path_train = generate_Data_To_Train(
        path_write, array_data_average, train_path
    )  # draw 5.0

    #Train(path_train, test_path, direction, colums_true_count, 20)  # draw 6.0

    #########################################------------------#########################################------------------#########################################
    # В следващите 50 реда код правим Тестов файл, обработките са същите както за файла за трениране, не изпозваме целия файл, само зададените евентите (записваме всяка стъпка в csv, всичко което правим преди го правим и за тестовия файл, без тренирането. После тренираме със файла за трениране и тестваме с тестовия.
    #############----------------------############################################################

    columns = {
        "AF3": False,
        "F7": False,
        "F3": False,
        "F5": False,
        "T7": False,
        "P7": False,
        "O1": False,
        "O2": False,
        "P8": False,
        "T8": False,
        "FC6": False,
        "F4": False,
        "F8": False,
        "AF4": False,
    }
    checkColumns(columns_input, columns)

    time, epoch = [], []
    af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4 = (
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    )

    event_id = []
    allow_write = False
    first_line = ""
    first_line_dsp = ""

    path_open, path_write = getData_less()

    (
        first_line_dsp,
        first_line,
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
    ) = open_first_file(
        path_open,
        allow_write,
        time,
        epoch,
        columns,
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
        event_id,
        first_line_dsp,
        first_line,
        condition,
        condition_after_event_ID,
    )

    raw_data_path = path_write + "RawData.csv"

    create_csv(
        af3,
        f7,
        f3,
        f5,
        t7,
        p7,
        o1,
        o2,
        p8,
        t8,
        fc6,
        f4,
        f8,
        af4,
        raw_data_path,
        event_id,
        time,
        epoch,
        first_line,
    )

    colums_true_count = 0
    filtered_signals = []

    if number_action != "2":
        colums_true_count, raw_data_path = generate_Butter_Data(
            path_write,
            first_line,
            af3,
            f7,
            f3,
            f5,
            t7,
            p7,
            o1,
            o2,
            p8,
            t8,
            fc6,
            f4,
            f8,
            af4,
            lowcut,
            highcut,
            fs,
            filter_order,
            filtered_signals,
            time,
            epoch,
            event_id,
            colums_true_count,
            columns_input,
        )

    array_data_average = []
    colums_true_count = get_columns_true_count(
        af3, f7, f3, f5, t7, p7, o1, o2, p8, t8, fc6, f4, f8, af4
    )
    if number_action != "1":
        first_line_DSP = write_DSP_Calculated_Data(
            offset,
            epoch_duration,
            condition,
            array_data_average,
            path_write,
            first_line_dsp,
            raw_data_path,
            colums_true_count,
        )

    path_test = generate_Data_To_Train(
        path_write, array_data_average, train_path, without_names=True
    )

    Train(path_train, path_test, direction, colums_true_count, rows_to_chunk_tests)


main()
