import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score


def Train(train_path, test_path, direction, colums_true_count, rows_to_chunk_tests):
    first_line_csv_results = ""

    def chunks(l, n):
        # For item i in a range that is a length of l,
        for i in range(0, len(l), n):
            # Create an index range for l of n items:
            yield l[i : i + n]

    def ShowHowCorrectTheDataIs(X_train, X_test, y_train, y_test):
        """
        Тестваме мрежата с данни от нея за да покаже колко е точна
        """

        # draw 6.5 ot
        sc = StandardScaler()
        X_train = sc.fit_transform(X_train)
        X_test = sc.transform(X_test)

        # ------
        lda = LDA(n_components=1, solver="svd", store_covariance=True)
        X_train = lda.fit_transform(X_train, y_train)
        X_test = lda.transform(X_test)

        # -------
        classifier = RandomForestClassifier(max_depth=2, random_state=0)

        classifier.fit(X_train, y_train)
        y_pred = classifier.predict(X_test)

        # -------
        cm = confusion_matrix(y_test, y_pred)

        # draw 6.5 do

        first_line_csv_results = (
            "Accuracy: "
            + str(round(float(accuracy_score(y_test, y_pred) * 100), 2))
            + "%"
        )

        print(first_line_csv_results)  # draw 6.6

        return first_line_csv_results

    def TestData():

        results_data = []

        lda = LDA(n_components=1)  # ????

        lda.fit(X, y)

        array_of_numbers = []
        array_of_array_of_numbers = []

        # Четем данните за тестване
        with open(test_path, "r") as file:  # draw 7.1

            for line in file:  # draw 7.2

                for number in line.split(","):  # draw 7.3
                    if (
                        number == ""
                        or number == "\n"
                        or number == "Left"
                        or number == "Right"
                    ):  # draw 7.4
                        continue

                    if number[-1] == "\n":  # draw 7.5
                        array_of_numbers.append(float(number[:-1]))  # draw 7.7
                        break
                    array_of_numbers.append(float(number))  # draw 7.6

                array_of_array_of_numbers.append(array_of_numbers)  # draw 7.8
                array_of_numbers = []

                # Chunkvame masiva
        list_chunks = list(
            chunks(array_of_array_of_numbers, int(rows_to_chunk_tests))
        )  # draw 7.9
        count_chunks = 0

        count_lines_start = 1
        count_lines_end = int(rows_to_chunk_tests)

        for chunk in list_chunks:  # draw 7.10

            array_test = chunk

            names = [[direction]] * len(array_test)

            # Тука е резултата в %
            percentage = round(lda.score(array_test, names), 3) * 100  # draw 7.11

            # zapazvame rezultata ako iskame posle da go zapishem v csv
            # results_data.append( "Chunk: " + str(count_chunks) + ",is " + str(percentage) + "% " + direction + ",Start line: " + str(count_lines_start) + ",End line: " + str(count_lines_end) + "\n")

            print(
                "Chunk: "
                + str(count_chunks)
                + " is "
                + str(percentage)
                + "% "
                + direction
                + " -  Start line: "
                + str(count_lines_start)
                + "  -  End line: "
                + str(count_lines_end)
            )  # draw 7.12
            count_chunks += 1
            count_lines_start = count_lines_end + 1
            count_lines_end += int(rows_to_chunk_tests)

        return results_data

    url = train_path

    names = [
        "Class",
        "AF3",
        "F7",
        "F3",
        "F5",
        "T7",
        "P7",
        "O1",
        "O2",
        "P8",
        "T8",
        "FC6",
        "F4",
        "F8",
        "AF4",
        "",
    ]
    dataset_training_data = pd.read_csv(url, names=names)  # draw 6.1

    y = dataset_training_data.iloc[:, 0].values  # draw 6.2
    X = dataset_training_data.iloc[:, 1 : colums_true_count + 1].values  # draw 6.2

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=0
    )  # draw 6.3

    first_line_csv_results = ShowHowCorrectTheDataIs( X_train, X_test, y_train, y_test )  # draw 6.4
    print()

    if test_path != "Empty":  # draw 6.7
        results_data = TestData()  # draw 7.0

    # Za da zapishem rezultata v csv
    # with open(r"C:\Users\yane\Desktop\Results.csv", "wt") as out:
    #    first_line_csv_results += "\n"
    #    out.write(first_line_csv_results)
    #    for item in results_data:
    #        out.write(item)
    #
