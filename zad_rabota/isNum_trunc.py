def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def truncate(f, n):
    s = "{}".format(f)
    if "e" in s or "E" in s:
        return "{0:.{1}f}".format(f, n)
    i, p, d = s.partition(".")
    return ".".join([i, (d + "0" * n)[:n]])
